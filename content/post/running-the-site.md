---
title: "Running the site"
date: 2017-10-15T13:54:18+02:00
draft: false
tags: ["programming"]
---

This site is generated using [hugo](http://gohugo.io/), a static site generator.
The content is written in markdown and hosted on [GitLab Pages](http://pages.gitlab.io/).

You can see the [source code](https://gitlab.com/zunin/portfolio) on gitlab.

Each time new code (like a new post) is added to the repository, gitlab is set
up to generate the website and publish it on GitLab Pages right away.

## First deployment
To set up the first deployment of the site,
I used the `.gitlab-ci.yml` template for Hugo that included a simple setup for deployment of Hugo to gitlab pages.

After the first successful build of the website through the gitlab pipeline,
the publicly addressable URL, http://zunin.gitlab.io/portfolio/, showed a 404 error site. Searching the web I found that [other have had the same problem][gitlab-pages-404] and found that it had to do with the fact that I had installed
the theme as a git submodule. After fixing this, the site updated right away.

I fixed the submodule issue by adding the following to my `gitlab-ci.yml`:
```bash
variables:
    GIT_SUBMODULE_STRATEGY: normal  # non-recursive sub modules, needed for theme
```

[gitlab-pages-404]: https://discourse.gohugo.io/t/problem-with-building-gitlab-pages/4745/7 "Problem with building GitLab Pages"

## Automatic spell check
It turns out that sometimes I don't spell things correctly. To help me I decided to use a lukeapage's tool [node-markdown-spellcheck][md-spellcheck-github]. Furthermore I decided
to incorporate this tool into the Gitlab pipeline.
I did this using tmaier's [docker image][md-spellcheck-docker].

Because I am writing text using markdown and all
of the content of the site lives in the content folder, I only wanted to check that. With that in mind I added the following to my `gitlab-ci.yml`: 
```bash
spellcheck:
      image: docker:latest
      services:
        - docker:dind
      script:
        docker run -v $(pwd):/workdir tmaier/markdown-spellcheck:latest --report --ignore-numbers "content/**/*.md"
```

[md-spellcheck-github]: https://github.com/lukeapage/node-markdown-spellcheck "node-markdown-spellcheck on Github"

[md-spellcheck-docker]: https://hub.docker.com/r/tmaier/markdown-spellcheck/ "Docker image for node-markdown-spellcheck"