[![pipeline status](https://gitlab.com/zunin/portfolio/badges/master/pipeline.svg)](https://gitlab.com/zunin/portfolio/commits/master)


# Portfolio
This is the project to build my portfolio served at http://zunin.gitlab.io/portfolio/.
It is built using hugo and the gitlab pipeline and served on gitlab pages.

It is very much a work in progress at the moment, but still functional.